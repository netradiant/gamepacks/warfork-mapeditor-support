# Warfork map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Warfork.

This gamepack is based on the `netradiant_warfork_gamepack.zip` game pack provided by Warfork.

It is assumed to be GPLv2-licensed because it is based on Warsow game pack which is distributed under GPLv2 license.
